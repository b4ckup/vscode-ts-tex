SKIP=false
PROD=false
VERSION=""
while (( "$#" )); do
  case "$1" in
   -p|--publish)
      PROD=true
      shift
      ;;
    -s|--skip)
      SKIP=true
      shift
      ;;
    -v|--version)
      VERSION=$2
      shift 2
      ;;
    --) # end argument parsing
      shift
      break
      ;;
    -*|--*=) # unsupported flags
      echo "Error: Unsupported flag $1" >&2
      exit 1
      ;;
    *) # preserve positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done

echo "@@ Resolving dependencies"
npm install
echo "@@ Bundling extension"
webpack --mode production

if [ "$SKIP" = true ] ; then
  echo "@@ Building vsix packge"
  rm vscode-ts-tex-*.vsix
  vsce package
fi

if [ "$PROD" = true ] ; then
  echo "@@ Publishing extension"
  rm vscode-ts-tex-*.vsix
  vsce publish $VERSION
fi