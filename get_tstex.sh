#! /bin/bash
projectdir=$MyProjectDir
tstexdir="$projectdir/ts-tex"
tstexbuilddir="$tstexdir/dist"
moduledir="$projectdir/vscode-ts-tex/node_modules/ts-tex"

rm -r $moduledir
mkdir $moduledir

curdir=$PWD
cd $tstexdir
echo "starting build"
bash build.sh
cd $curdir
echo "copying..."
cp -r "$tstexdir/dist" "$moduledir"
cp -r "$tstexdir/src" "$moduledir"
cp "$tstexdir/package.json" $moduledir
echo "done"
