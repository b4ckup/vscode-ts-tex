import * as vscode from 'vscode';
import { Extension } from './Extension';

export enum CodeActionId
{
    insertBefore, insertAfter
}

declare var diagnosticCollection: vscode.DiagnosticCollection;

export class CodeActionProvider implements vscode.CodeActionProvider 
{
    public static COMMAND_ID = "tstex_execute_codeaction";

    constructor(private _extension: Extension) 
    {
        
    }
    
    public provideCodeActions(document: vscode.TextDocument, range: vscode.Range, context: vscode.CodeActionContext, token: vscode.CancellationToken)
        : Thenable<vscode.Command[]> 
    {
      return null;
    }
}