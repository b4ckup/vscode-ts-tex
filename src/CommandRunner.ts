import * as cp from "child_process";
import * as vscode from 'vscode';
import { Extension } from "./Extension";
import * as path from 'path';

export class CommandRunner
{
  constructor(private extension: Extension)
  {

  }
  public runCommand(cmd: string, args: string[])
  {
    this.extension.terminal.show();
    this.extension.terminal.sendText("\n");
    let outfile = path.relative(this.extension.ast.workdir, this.extension.ast.outFile);
    args = args.map(a => {
      return a.replace("${outfile}", outfile);
    });
    this.extension.terminal.sendText(`${cmd} ${args.join(" ")}`);
  }
}