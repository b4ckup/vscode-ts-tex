import * as vscode from 'vscode';
import { Extension } from './Extension';
import { Provider } from './abstracts/Provider';

export class CompletionProvider extends Provider implements vscode.CompletionItemProvider
{
    constructor(ext: Extension)
    {
      super("CompletionProvider", ext);
    }

    validTriggerChars = [".",'"',"'","`","/","@","<"]
    
    provideCompletionItems(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken, context: vscode.CompletionContext): vscode.ProviderResult<vscode.CompletionItem[] | vscode.CompletionList> 
    {
      // function completionKindToVsCode(kind: ScriptElementKind): vscode.CompletionItemKind
      // {
      //   switch(kind)
      //   {
      //     case ScriptElementKind.
      //   }
      // }
      return new Promise((resolve, reject) => {
        try
        {
          let sf = this.ast.getSourceFile(document.uri.fsPath);
          if(sf == null)
          {
            this.logger.logInfo(`Cannot provide completion items for file ${document.uri.fsPath}, cannot find it in the ast`);
            resolve(null);
            return;
          }
          sf.update(document.getText())
          if(this.validTriggerChars.indexOf(context.triggerCharacter) >= 0)
          {
            let completions = this.languageService.getCompletionsAtPosition(document.offsetAt(position), document.uri.fsPath, context.triggerCharacter as any);
            if(completions == null)
            {
              resolve(null);
              return;
            }
            let res = completions.entries.map(e => {
              let c = new vscode.CompletionItem(e.name);
              c.insertText = e.insertText;
              c.detail = e.name + " (vscode-ts-tex)";
              return c;
            });
            resolve(res);
            return;
          }
        }
        catch(err)
        {
          this.logger.logError(`Error during providing of completion items`, err);
          resolve(null);
          return;
        }
      });
    }

    resolveCompletionItem?(item: vscode.CompletionItem, token: vscode.CancellationToken): vscode.ProviderResult<vscode.CompletionItem> 
    {
      return new Promise((resolve, reject) => {
        resolve(item);
        return;
      });
    }
}