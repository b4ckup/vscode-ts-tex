import * as vscode from "vscode";
import { Extension } from './Extension';
import { LanguageService, Ast, FnNode } from 'ts-tex';
import { Provider } from './abstracts/Provider';

export class DecorationProvider extends Provider
{
  private _decorators: vscode.TextEditorDecorationType[] = [];
  public indexDecoratorType = vscode.window.createTextEditorDecorationType({
      backgroundColor : 'rgba(255, 153, 102, 0.1)'
  });
  public highlightsymbols: boolean = false;
    
  constructor(extension: Extension)
  {
    super("DecorationProvider", extension);
  }

  public updateDecorations()
  {
    try
    {
      let document = vscode.window.activeTextEditor.document;
      if(document)
      {
        let file = this._extension.ast.getSourceFile(vscode.window.activeTextEditor.document.uri.fsPath);
        if(file)
        {
          this.clearDecorations();
          let nodes = this.languageService
            .getTsTexRootNodes(document.uri.fsPath)
            .filter(n => n.type == "fn") as FnNode[];
          
          if(this._extension.config.nodeDecoration == true)
          {
            let options= [];
            nodes.forEach(n => {
              let start = document.positionAt(n.range.start.offset);
              let end = document.positionAt(n.range.end.offset);
              let range = new vscode.Range(start, end);
              let opt = {range: range}
              options.push(opt);
            });
            vscode.window.activeTextEditor.setDecorations(this.indexDecoratorType, options);
          }
        }
      }
    }
    catch(error)
    {
      this.logger.logError("Error during updating of decorations", error);
    }
  }

  public clearDecorations()
  {
    vscode.window.activeTextEditor.setDecorations(this.indexDecoratorType, []);
  }
}