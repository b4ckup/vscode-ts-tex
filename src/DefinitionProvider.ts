import * as vscode from 'vscode';
import { Extension } from './Extension';
import { Ast, LanguageService } from 'ts-tex';
import * as fs from "fs";
import * as path from 'path';
import { Provider } from './abstracts/Provider';
import { ReturnStatement } from 'ts-morph';

export class DefinitionProvider extends Provider implements vscode.DefinitionProvider 
{
    constructor(ext: Extension)
    {
        super("DefinitionProvider", ext);
    }

    provideDefinition(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken): vscode.ProviderResult<vscode.Definition> 
    {
        return new Promise<vscode.Definition>((resolve, reject) => {
            try
            {
              let def = this.languageService.getDefinitionAtPosition(document.offsetAt(position), document.uri.fsPath);
              if(def.length > 0)
              {
                let d = def[0];
                let file = path.normalize(d.fileName);
                let doc = vscode.workspace.textDocuments.find(t => path.normalize(t.uri.fsPath) == file);
                if(!doc)
                {
                  vscode.workspace.openTextDocument(file)
                  .then(doc => {
                      resolve(new vscode.Location(vscode.Uri.file(file), new vscode.Range(doc.positionAt(d.textSpan.start), doc.positionAt(d.textSpan.start+d.textSpan.length))));
                      return;
                    }, rej => {
                      this.logger.logError(`Cannot open document ${file}`, rej);
                      resolve(null);
                      return;
                  });
                }
                else
                {
                  resolve(new vscode.Location(vscode.Uri.file(file), new vscode.Range(doc.positionAt(d.textSpan.start), doc.positionAt(d.textSpan.start+d.textSpan.length))));
                  return;
                }
              }
              else
              {
                resolve(null);
                return;
              }
            }
            catch(error)
            {
              this.logger.logError("Cannot provide definition", error);
              resolve(null);
              return;
            }
        });
    }
}