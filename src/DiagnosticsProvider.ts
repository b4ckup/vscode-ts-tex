import { Extension } from "./Extension";
import * as vscode from 'vscode';
import * as path from "path";
import * as ts_ast from "ts-morph";
import { Provider } from './abstracts/Provider';

export class DiagnosticsProvider extends Provider
{
    public diagnosticCollection = vscode.languages.createDiagnosticCollection('ts-tex');


    constructor(ext: Extension)
    {
      super("DiagnosticProvider", ext);
    }
        
    public updateDiagnostics()
    {
      for(let file of this.ast.sourceFiles.map(s => s.path))
      {
        this.updateDiagnosticsForFile(file);
      }
    }

    public updateDiagnosticsForFile(file: string)
    {
      function categoryToDiagnosticSeverity(cat: ts_ast.DiagnosticCategory): vscode.DiagnosticSeverity
      {
        switch(cat)
        {
          case ts_ast.DiagnosticCategory.Error:
            return vscode.DiagnosticSeverity.Error;
          case ts_ast.DiagnosticCategory.Message:
            return vscode.DiagnosticSeverity.Information;
          case ts_ast.DiagnosticCategory.Suggestion:
            return vscode.DiagnosticSeverity.Hint;
          case ts_ast.DiagnosticCategory.Warning:
            return vscode.DiagnosticSeverity.Warning;
          default:
            return vscode.DiagnosticSeverity.Error;
        }
      }

      try
      {
        file = path.normalize(file);
        let uri = vscode.Uri.file(file);
        let diagnostics: ts_ast.ts.Diagnostic[] = this.languageService.getDiagnosticsForFile(file);
        if(diagnostics == null)
        {
          this.logger.logWarn(`Got Diagnostic for sourcefile ${file} but cannot find it in Ast`);
          this.diagnosticCollection.set(uri, undefined);
        }
        else
        {
          let doc = vscode.workspace.textDocuments.find(d => d.uri.fsPath == uri.fsPath);
          if(!doc)
          {
            this.logger.logInfo(`Document ${file} not found, cannot provide diagnostics. Maybe the file just isn't open?`);
            return; 
          }
  
          let mapped: vscode.Diagnostic[] = diagnostics.map(d => {
            return {
              range: new vscode.Range(doc.positionAt(d.start), doc.positionAt(d.start + d.length)),
              message: d.messageText.toString(),
              source: d.source,
              severity: categoryToDiagnosticSeverity(d.category),
              code: d.code
            };
          });
          this.diagnosticCollection.set(uri, mapped);
        }
      }
      catch(error)
      {
        this.logger.logError(`Error during updating of diagnostics for file ${file}`, error);
      }

    }
}