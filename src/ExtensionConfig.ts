import * as vscode from 'vscode';

export class ExtensionConfig
{
  postbuild: { program: string, args: string[] } = null;
  buildOnSave: boolean = true;
  nodeDecoration: boolean = true;
  rootFile: string;
  
  constructor()
  {
    let config = vscode.workspace.getConfiguration("tstex");
    Object.assign(this, config);
  }
}