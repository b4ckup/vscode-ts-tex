import * as vscode from 'vscode';
import * as os from "os";
import * as cp from "child_process";
import { Extension } from './Extension';
import { Provider } from './abstracts/Provider';
import { NodeExecutionError, RootNode } from 'ts-tex';

export class HoverProvider extends Provider implements vscode.HoverProvider 
{
  constructor(ext: Extension)
  {
    super("HoverProvider", ext);
  }
  
  public provideHover(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken): Thenable<vscode.Hover> 
  {
    return new Promise<vscode.Hover>((resolve, reject) => {
      function createHover(node: RootNode, result: string)
      {
        return new vscode.Hover("```latex\n"+ result + "\n```" , new vscode.Range(document.positionAt(node.range.start.offset), document.positionAt(node.range.end.offset)));
      }
      try
      {
        let pos = document.offsetAt(position);
        let result = this.languageService.executeNodeAtPosition(pos, document.uri.fsPath);
        if(result != null)
          result = result.trim();
        let node = this.languageService.getTsTexNodeAt(pos, document.uri.fsPath);
        resolve(createHover(node, result));
        return;
      }
      catch(error)
      {
        if(error instanceof NodeExecutionError)
        {
          let str = `This node could not be evaluated. Message: ${error.message}`;
          if(error.innerError != null)
            str += `\nInnerError: ${error.innerError.message}`;
          resolve(new vscode.Hover(str));
          return;
        }
        else
        {
          // this.logger.logError("Error during node execution", error);
          resolve(null);
          return;
        }
      }
    });
  }
}

