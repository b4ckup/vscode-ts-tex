export enum LogLevel 
{
  Verbose = 0, Debug, Info, Warn, Error, Fatal
}

export class Logger
{
  component: string;
  private _logLambda: (component: string, message: string, level: LogLevel, error?: any) => void;

  constructor(componentName: string, logLambda: (component: string, message: string, level: LogLevel, error?: any) => void)
  {
    this.component  = componentName;
    this._logLambda = logLambda;
  }

  log(message: string, level: LogLevel, error: any = null)
  {
    this._logLambda(this.component, message, level, error);
  }
  logVerbose(message: string, error: any = null)
  {
    this.log(message, LogLevel.Verbose, error);
  }
  logDebug(message: string, error: any = null)
  {
    this.log(message, LogLevel.Debug, error);
  }
  logInfo(message: string, error: any = null)
  {
    this.log(message, LogLevel.Info, error);
  }
  logWarn(message: string, error: any = null)
  {
    this.log(message, LogLevel.Warn, error);
  }
  logError(message: string, error: any = null)
  {
    this.log(message, LogLevel.Error, error);
  }
  logFatal(message: string, error: any = null)
  {
    this.log(message, LogLevel.Fatal, error);
  }


}