import { Extension } from "./Extension";
import * as vscode from 'vscode';
import { LanguageService } from 'ts-tex';
import { Provider } from "./abstracts/Provider";

export class SignatureHelpProvider extends Provider implements vscode.SignatureHelpProvider 
{
  get languageService(): LanguageService {
    return this._extension.ast.languageService;
  }
  
  constructor(ext: Extension)
  {
      super("SignatureHelpProvider", ext)
  }

  provideSignatureHelp(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken): vscode.ProviderResult<vscode.SignatureHelp> 
  {
    return new Promise((resolve, reject) => {
      try
      {
        let sf = this._extension.ast.getSourceFile(document.uri.fsPath);
        sf.update(document.getText());
        let signature = this.languageService.getSignatureHelpAtPosition(document.offsetAt(position), document.uri.fsPath, "(");
        if(signature != null)
        {
          let res =  new vscode.SignatureHelp()
          res.activeParameter = signature.argumentIndex;
          res.activeSignature = signature.selectedItemIndex;

          res.signatures = signature.items.map(s => {
            let label = "(" + s.parameters.map(c => c.displayParts.map(d => d.text).join("")).join(", ") + ")";
            let info = new vscode.SignatureInformation(label, s.documentation.map(d => d.text).join("\n"));
            info.parameters = s.parameters.map(p => {
              return new vscode.ParameterInformation(p.displayParts.map(d => d.text).join(""), p.documentation.map(d => d.text).join("\n"));
            });
            return info;
          });
          resolve(res);
          return;
        }
        else
        {
          this.logger.logError(`Signature is null for doc ${document.uri.fragment}, position: ${position}`);
          resolve(null);
          return;
        }
      }
      catch(error)
      {
        this.logger.logError(`Cannot provide signature for doc ${document.uri.fragment}, position: ${position}`, error);
        resolve(null);
        return;
      }
    });
  }
}