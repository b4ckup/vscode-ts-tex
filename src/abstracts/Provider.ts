import { Extension } from '../Extension';
import { LanguageService, Ast } from 'ts-tex';
import { Logger } from '../Logging';

export abstract class Provider 
{
  get languageService(): LanguageService {
    return this._extension.ast.languageService;
  }
  get ast(): Ast {
    return this._extension.ast;
  }
  protected _extension: Extension;
  protected logger: Logger

  constructor(name: string, ext: Extension)
  {
    this._extension = ext;
    this.logger = ext.getLogger(name);
  }
}