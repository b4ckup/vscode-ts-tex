'use strict';
import * as vscode from 'vscode';
import { DecorationProvider } from './DecorationProvider';
import { Ast, Global, NodeExecutionError, CRLFError, LifecycleHookError } from 'ts-tex';
import { DiagnosticsProvider } from './DiagnosticsProvider';
import { CompletionProvider } from './CompletionProvider';
import { SignatureHelpProvider } from './SignatureHelpProvider';
import { HoverProvider } from './HoverProvider';
import { DefinitionProvider } from './DefinitionProvider';
import { CodeActionProvider } from './CodeActionProvider';
import * as path from "path";
import { LogLevel, Logger } from './Logging';
import { ExtensionConfig } from './ExtensionConfig';
import { CommandRunner } from './CommandRunner';

const TSTEX_GLOB_PATTERN = "**/*.tstex";


export class Extension
{
  ast: Ast;
  decorationProvider    = new DecorationProvider(this);
  diagnosticsProvider   = new DiagnosticsProvider(this);
  completionProvider    = new CompletionProvider(this);
  signatureHelpProvider = new SignatureHelpProvider(this);
  hoverProvider         = new HoverProvider(this);
  definitionProvider    = new DefinitionProvider(this);
  codeActionProvider    = new CodeActionProvider(this);
  outputChannel         = vscode.window.createOutputChannel("ts-tex");
  config                = new ExtensionConfig();
  commandRunner         = new CommandRunner(this);

  private _terminal: vscode.Terminal;
  get terminal(): vscode.Terminal{
    if(this._terminal == null)
      this._terminal = vscode.window.createTerminal("ts-tex");
    return this._terminal;
  }

  tstexWatcher: vscode.FileSystemWatcher;
  codeFileWatcher: vscode.FileSystemWatcher;

  minimumDisplayLogLevel = LogLevel.Error;
  minimumPrintLogLevel = LogLevel.Info;
  minimumOutputChannelLogLevel = LogLevel.Info;

  private logger = this.getLogger("Extension");

  constructor()
  {
  }

  async addSourceFile(p: string)
  {
    try 
    {
      let sf = await this.ast.addSourceFile(p);
      sf.ensureNoCRLF();
    }
    catch(error)
    {
      if(error instanceof CRLFError)
      {
        this.logger.logError(error.toString());
      }
    }
  }

  updateConfig()
  {
    this.config = new ExtensionConfig();
    this.decorationProvider.updateDecorations();
  }

  addCodeFile(uri: vscode.Uri)
  {
    if(this.ast.apiFile.path == path.resolve(uri.fsPath))
      return;
    try 
    {
      this.ast.addCodeFile(uri.fsPath);
    } 
    catch (error) 
    {
      if(error instanceof LifecycleHookError)
      {
        this.logger.logError(`Component ${error.componentname} threw an error on lifecycle hook ${error.kind}: ${error.innerError.stack}`)
      }  
    }
  }

  removeCodeFile(uri: vscode.Uri)
  {
    if(this.ast.apiFile.path == path.resolve(uri.fsPath))
      return;
    try 
    {
      this.ast.removeCodeFile(uri.fsPath);
    } 
    catch (error) 
    {
      if(error instanceof LifecycleHookError)
      {
        this.logger.logError(`Component ${error.componentname} threw an error on lifecycle hook ${error.kind}: ${error.innerError.stack}`)
      }  
    }
  }

  refreshCodeFile(uri: vscode.Uri)
  {
    if(this.ast.apiFile.path == path.resolve(uri.fsPath))
      return;
    try 
    {
      this.ast.refreshCodeFile(uri.fsPath);
    } 
    catch (error) 
    {
      if(error instanceof LifecycleHookError)
      {
        this.logger.logError(`Component ${error.componentname} threw an error on lifecycle hook ${error.kind}: ${error.innerError.stack}`)
      }  
    }
  }

  public async init(context: vscode.ExtensionContext)
  {
    try
    {
      if(vscode.workspace.workspaceFolders.length < 1)
      {
        this.logger.logWarn("No folder open, cannot initialize vscode-ts-tex!");
        return;
      }

      this.ast = new Ast(vscode.workspace.workspaceFolders[0].uri.fsPath);
      this.ast.logger = (m, l, e) => {
        this.log("ts-tex", m, l as LogLevel, e);
      };
      if(this.config.rootFile != null)
        this.ast.rootFile = this.config.rootFile;

      this.tstexWatcher = vscode.workspace.createFileSystemWatcher(new vscode.RelativePattern(this.ast.workdir, TSTEX_GLOB_PATTERN))
      this.tstexWatcher.onDidCreate(uri => {
        this.addSourceFile(uri.fsPath);
      }, this);
      this.tstexWatcher.onDidDelete(uri => {
        this.ast.removeSourceFile(uri.fsPath);
      }, this);
      this.tstexWatcher.onDidChange(uri => {
        let doc = vscode.workspace.textDocuments.find(d => d.uri.fsPath == uri.fsPath);
        if(this.config.buildOnSave == true &&  this.ast.rootFile != null && doc != null)
        {
          this.updateSourceFile(doc);
          this.build();
        }
      }, this);
      
      this.codeFileWatcher = vscode.workspace.createFileSystemWatcher(new vscode.RelativePattern(this.ast.moduledir, "**/*.ts"));
      this.codeFileWatcher.onDidChange(uri => {
        this.refreshCodeFile(uri);
      }, this);
      this.codeFileWatcher.onDidCreate(uri => {
        this.addCodeFile(uri);
      }, this);
      this.codeFileWatcher.onDidDelete(uri => {
        this.removeCodeFile(uri);
      }, this);

      let files = await vscode.workspace.findFiles(TSTEX_GLOB_PATTERN);
      files.forEach(async f => {
        this.logger.logInfo(`Adding sourcefile ${f.fsPath}`);
        await this.addSourceFile(f.fsPath);
      });
      files = await vscode.workspace.findFiles(`${Global.DIRNAME_TSTEXMODULES}/*.ts`);
      files.forEach(f => {
        this.logger.logInfo(`Adding codefile ${f.fsPath}`);
        this.addCodeFile(f);
      });
      this.logger.logVerbose("Initializing diagnostics");
      this.diagnosticsProvider.updateDiagnostics();
      this.logger.logVerbose("Initializing decorations");
      this.decorationProvider.updateDecorations();
      this.showMessage("vscode-ts-tex activated");
    }
    catch(error)
    {
      this.logger.logError(`Error during intialization of vscode-ts-tex`, error);
    }
  }

  public getLogger(componentName: string): Logger
  {
    return new Logger(componentName, this.log.bind(this));
  }

  public disposeResources()
  {
    this.codeFileWatcher.dispose();
    this.tstexWatcher.dispose();
  }

  public log(component: string, message: string, level: LogLevel, error: any = null)
  {
    let d = new Date();
    let m = `[${d.getHours()}:${d.getMinutes()}] ${LogLevel[level]} - ${component}: ${message} ${error != null ? error : ""}`;
    if(level >= this.minimumPrintLogLevel)
    {
      console.log(m);
    }
    if(level >= this.minimumOutputChannelLogLevel)
    {
      this.outputChannel.appendLine(m);
    }
    // if(level >= this.minimumDisplayLogLevel)
    // {
    //   if(level == LogLevel.Warn || level == LogLevel.Error || level == LogLevel.Fatal)
    //   {
    //     vscode.window.showErrorMessage(`vscode-ts-tex reports an Error:
    //     Level: ${LogLevel[level]}
    //     Component: ${component}
    //     Message: ${message}
    //     ${error != null ? error.message : ""}
    //     Stack: ${error != null ? error.stack : ""}`)
    //   }
    //   else
    //   {
    //     vscode.window.showInformationMessage(`vscode-ts-tex ${component}: ${message}`);
    //   }
    // }
  }

  public build()
  {
    if(this.ast.rootFile == null)
    {
      this.showConfigureRootFileDialog().then(s => {
        this.build();
      })
    }
    else
    {
      try
      {
        this.logger.logInfo("Starting ts-tex build....");
        let start = new Date().getTime();
        let doc = vscode.window.activeTextEditor.document;
        this.ast.emit();
        let end = new Date().getTime();
        this.logger.logInfo(`Build successfull, took ${(end-start)/1000}s!`);
        this.outputChannel.show();
        vscode.window.showTextDocument(doc);
        if(this.config.postbuild != null)
        {
          this.logger.logInfo(`Running postbuild command...`);
          this.commandRunner.runCommand(this.config.postbuild.program, this.config.postbuild.args);
        }
      }
      catch(error)
      {
        if(error instanceof NodeExecutionError)
        {
          this.logger.logError(error.toString());
        }
        else if(error instanceof LifecycleHookError)
        {
          this.logger.logError(error.toString());
        }
        else
        {
          let msg = `ts-tex encountered unknown build error: ${error.message}\n Stack: ${error.stack}`;
          this.logger.logError(msg, error);
        }
      }
    }
  }

  public showMessage(message: string): void
  {
      vscode.window.showInformationMessage(message);
  }

  public showError(message: string): void
  {
      vscode.window.showErrorMessage(message);
  }

  public updateSourceFile(doc: vscode.TextDocument)
  {
    try
    {
      this.logger.logVerbose(`Updating document ${doc.uri.fsPath}`)
      let sf = this.ast.getSourceFile(doc.uri.fsPath);
      sf.update(doc.getText());
      this.diagnosticsProvider.updateDiagnosticsForFile(sf.path);
      if(doc == vscode.window.activeTextEditor.document)
        this.decorationProvider.updateDecorations();
    }
    catch(error)
    {
      this.logger.logError(`Caught error during update of document ${doc.uri.fsPath}`, error); 
    }
  }

  public async showConfigureRootFileDialog(): Promise<{}>
  {
    return new Promise((resolve, reject) => {
      vscode.window.showQuickPick(this.ast.sourceFiles.map(d => path.relative(this.ast.workdir, d.path)), {
        placeHolder: "Select Document RootFile"
      }).then(s => {
        this.ast.rootFile = path.resolve(this.ast.workdir, s);
        vscode.workspace.getConfiguration("tstex").update("rootFile", this.ast.rootFile, vscode.ConfigurationTarget.Workspace);
        resolve();
        return;
      });
    })
  }

  private async getDocument(file: string): Promise<vscode.TextDocument>
  {
    file = path.normalize(file);
    let doc = vscode.workspace.textDocuments.find(d => path.normalize(d.uri.fsPath) == file);
    if(doc)
    {
      return doc;
    }
    else
    {
      return vscode.workspace.openTextDocument(file);
    }
  }
}
