import * as vscode from "vscode";
import * as path from 'path';
import { Extension } from "./Extension";

var extension: Extension;

export function activate(context: vscode.ExtensionContext) 
{
  console.log("activating vscode-ts-tex");
  extension = new Extension();
  extension.init(context).then(success => {
    this.logger.logInfo("vscode-ts-tex successfully initialized");
  }, rej => {
    this.logger.logInfo("error during intialization of vscode-ts-tex: "+rej)
  });

  let changeTimeout;
  context.subscriptions.push(vscode.commands.registerCommand('tstex.build', async () => 
  {
      extension.build();
  }));

  context.subscriptions.push(vscode.commands.registerCommand('tstex.rewriteApiFile', async () => 
  {
      extension.ast.apiFile.rewrite();
  }));

  context.subscriptions.push(vscode.commands.registerCommand('tstex.configureRoot', async () => 
  {
    extension.showConfigureRootFileDialog();
  }));

  context.subscriptions.push(vscode.commands.registerCommand('tstex.reload', async () => 
  {
    extension.disposeResources();
    extension.init(context).then(success => {
      this.logger.logInfo("vscode-ts-tex successfully initialized");
    }, rej => {
      this.logger.logInfo("error during intialization of vscode-ts-tex: "+rej)
    });
  }));

  context.subscriptions.push(vscode.workspace.onDidChangeConfiguration((c) => {
    if(c.affectsConfiguration("tstex") == true)
    {
      extension.updateConfig();
    }
  }));

  context.subscriptions.push(vscode.workspace.onDidChangeTextDocument(e => {
    clearTimeout(changeTimeout);
    changeTimeout = setTimeout(() => {
      if((e.document.languageId == "latex" || e.document.languageId == "tstex") && path.basename(e.document.uri.fsPath).startsWith(".") == false)
      {
        extension.updateSourceFile(e.document);
      }
    }, 500);
  }));

  context.subscriptions.push(vscode.window.onDidChangeActiveTextEditor(e => {
    extension.decorationProvider.updateDecorations();
  }));

  context.subscriptions.push(vscode.workspace.onDidChangeWorkspaceFolders(e => {
    extension.disposeResources();
    extension.init(context).then(success => {
      console.log("vscode-ts-tex successfully initialized");
    }, rej => {
      console.log("error during intialization of vscode-ts-tex: "+rej)
    });
  }));

  context.subscriptions.push(vscode.workspace.onDidOpenTextDocument(doc => {
    if((doc.languageId == "tstex") && path.basename(doc.uri.fsPath).startsWith(".") == false)
    {
      if(extension.ast.getSourceFile(doc.uri.fsPath) == null)
        extension.addSourceFile(doc.uri.fsPath);
      else
        extension.updateSourceFile(doc);
    }
  }));

  context.subscriptions.push(vscode.languages.registerCompletionItemProvider({scheme: "file", language: "tstex" }, extension.completionProvider, ".", "\""));
  context.subscriptions.push(vscode.languages.registerSignatureHelpProvider({scheme: "file", language: "tstex" }, extension.signatureHelpProvider, "(", ",", ")"));
  context.subscriptions.push(vscode.languages.registerHoverProvider({scheme: "file", language: "tstex" }, extension.hoverProvider));
  context.subscriptions.push(vscode.languages.registerDefinitionProvider({scheme: "file", language: "tstex" }, extension.definitionProvider));
  context.subscriptions.push(vscode.languages.registerCodeActionsProvider({scheme: "file", language: "tstex" }, extension.codeActionProvider));    
}


export function deactivate() {
  extension.disposeResources();
  extension = null;
}